[![Website](https://img.shields.io/website/https/swc-public.pages.rwth-aachen.de/ea-refactoring-solutions/web-catalog/.svg.svg?down_message=offline&up_message=online)](https://swc-public.pages.rwth-aachen.de/ea-refactoring-solutions/web-catalog/)
[![pipeline status](https://git.rwth-aachen.de/swc-public/ea-refactoring-solutions/web-catalog/badges/master/pipeline.svg)](https://git.rwth-aachen.de/swc-public/ea-refactoring-solutions/web-catalog/-/commits/master)
[![Contributions welcome](https://img.shields.io/badge/contributions-welcome-orange.svg)](https://git.rwth-aachen.de/swc-public/ea-refactoring-solutions/web-catalog/-/blob/master/CONTRIBUTING.md)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)
# Enterprise Architecture Refactoring

This repository should serve as a knowledge base for Enterprise Architecture Refactorings.

The first idea of the web-application  was first developed by Bogner et al., and is now adapted to provide information on Enterprise Architecture Smells.

The repository has been created
1. to collect and browse refactorings in a structured and uniform way (JSON format)
2. to give researchers and practitioners an easy way to access the collection and contribute their refactorings
3. to version the results


Available under [https://swc-public.pages.rwth-aachen.de/ea-refactoring-solutions/web-catalog/](https://swc-public.pages.rwth-aachen.de/ea-refactoring-solutions/web-catalog/)
